package com.singularitybd.smsinterceptapp

import android.os.AsyncTask
import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.joda.time.DateTime
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL

/**
 * Created by Sadman Sarar on 5/9/18.
 */


class SmsReceivedAsyncTask(
        private val msg: String,
        private val number: String,
        private val time: String,
        private val url: String = "http://bondstein.net"
) : AsyncTask<String, String, String>() {

    val BASE_URL = if (url.isEmpty()) {
        "http://bondstein.net"
    } else {
        val _url = URL(url);

        _url.protocol + "://" + _url.host
    }

    val URL_PATH = if (url.isEmpty()) {
        "sinotrack/api_sms_recv.php"
    } else {
        val _url = URL(url);

        _url.path.removePrefix("/")
    }


    override fun doInBackground(vararg p0: String?): String {
        val api = webService()
        try {
            val response = api.postSms(URL_PATH,msg, number, time).execute()
            if (response.isSuccessful) {
                Log.d("SMS","Success")
                return ""
            }
            Log.d("SMS","Error ___")
        } catch (ex: Exception) {
            Log.d("SMS","Error")
        }
        return ""
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
    }

    fun webService(): WebService {

        val okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.addNetworkInterceptor(StethoInterceptor())


        val builder = GsonBuilder()
        val gsonFactory = GsonConverterFactory.create(builder.create())

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(gsonFactory)
                .client(okHttpBuilder.build())
                .build()
                .create(WebService::class.java)
    }

}