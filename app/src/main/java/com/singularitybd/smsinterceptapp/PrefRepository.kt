package com.singularitybd.smsinterceptapp

import android.content.Context

class PrefRepository(private val mContext: Context) {
    private val APP_SHARED_PREFERENCE = "com.singularitybd.smsinterceptapp.sp"

    companion object {
        private val SP_URL = "SP_URL"
    }

    fun saveBoolToSP(data: Boolean, code: String) {
        val sp = mContext.getSharedPreferences(APP_SHARED_PREFERENCE, Context.MODE_PRIVATE)
        val e = sp.edit()
        e.putBoolean(code, data)
        e.apply()
    }

    fun getBoolFromSP(code: String): Boolean {
        val sp = mContext.getSharedPreferences(APP_SHARED_PREFERENCE, Context.MODE_PRIVATE)
        return sp.getBoolean(code, false)
    }


    fun saveStringToSP(data: String, code: String) {
        val sp = mContext.getSharedPreferences(APP_SHARED_PREFERENCE, Context.MODE_PRIVATE)
        val e = sp.edit()
        e.putString(code, data)
        e.apply()
    }

    fun getStringFromSP(code: String): String {
        val sp = mContext.getSharedPreferences(APP_SHARED_PREFERENCE, Context.MODE_PRIVATE)
        return sp.getString(code, "")
    }

    fun saveUrl(token: String?) {
        saveStringToSP(token ?: "", SP_URL)
    }

    fun getUrl(): String {
        return getStringFromSP(SP_URL)
    }
}